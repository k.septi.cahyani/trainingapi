<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>All Get List</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>5aaf6e85-4a27-4baa-829c-81d729818984</testSuiteGuid>
   <testCaseLink>
      <guid>8b90c75a-3239-4c21-8dc8-642abbd432a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Get Page 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>815c9120-24d6-4ca1-8804-cb343776cde1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Get Page 2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eb96341e-7afd-4519-91b8-92312fb0e452</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Get ID</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
